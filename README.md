# less7.1

containers(nginx + vts exporter+grafana+prometheus) в docker-compose + нагрузка yandex tank

ЗАДАНИЕ
1. Запустить nginx с vts exporter в docker, конфигурация nginx - отдача 200 'Live'. нагрузить его с помошью yandex tank, параметры: 1 минута, rps от 50 до 500.
 Снять метрики, предоставить ссылку на отчет yandex tank и скриншоты дашборда графаны за этот период

ОТВЕТ НА ЗАДАНИЕ
Используем doker-compose.yam для поднятия контейнеров nginx(с конфигом nginx/nginx.conf), x-Exporter, x-prometheus (c конфигом /monitor/prometheus.yml), grafana. Проверяем метрики по пути localhost:9090.
Импортируем data source(prometheus) в graphana и импортируем  dashboard( файл dashboard).
Запускаем контейнер yandex-tank командой: 

docker run -v $(pwd)/yandextank:/var/loadtest --name yandex-tank -it --net host --entrypoint /bin/bash direvius/yandex-tank, там запускаем yandex-tank -c load.yaml.

ГРАФИК в OverLoad: https://overload.yandex.net/505302#tab=test_data&tags=&plot_groups=main&machines=&metrics=&slider_start=1645615220&slider_end=1645615280
 
